﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HelloWorld
{
    /// <summary>
    /// Interaction logic for MijnWindow.xaml
    /// </summary>
    public partial class MijnWindow : Window
    {
        public MijnWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            var w = new MainWindow();
            w.Show();
        }

        private void PopulateListbox()
        {
            listBox.Items.Add("één");
            listBox.Items.Add("twee");
            listBox.Items.Add("drie");
        }

        private void populateListboxButton_Click(object sender, RoutedEventArgs e)
        {
            PopulateListbox();
        }

        private void SetBirthday()
        {
            var d = new DateTime(2000, 10, 1);
            calendar.DisplayDate = d;
        }

        private void birthdayButton_Click(object sender, RoutedEventArgs e)
        {
            SetBirthday();
        }
    }
}
