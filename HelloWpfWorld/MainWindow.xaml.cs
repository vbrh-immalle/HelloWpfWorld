﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HelloWorld
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Debug.WriteLine("Button clicked");
            halloLabel.Content = "EVENTS!!!";
            halloLabel.MouseDoubleClick += HalloLabel_MouseDoubleClick;
            halloLabel.Background = Brushes.Aqua;
        }

        private void HalloLabel_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            halloLabel.Background = new RadialGradientBrush(Color.FromRgb(255, 0, 0), Color.FromRgb(128, 0, 0));
        }

        private void button_Click_1(object sender, RoutedEventArgs e)
        {
            var w = new DemoWindow();
            w.Show();
        }

        private void button_KeyDown(object sender, KeyEventArgs e)
        {
            infoLabel.Content = "button_keyDown event";
            infoLabel.Content += " - Toets: " + e.Key.ToString();
        }

        private void button_MouseEnter(object sender, MouseEventArgs e)
        {
            infoLabel.Content = "button_MouseEnter event";
            infoLabel.Content += " - LeftButton: " + e.LeftButton.ToString();
        }
    }
}
