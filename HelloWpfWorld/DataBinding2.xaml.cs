﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace HelloWorld
{
    /// <summary>
    /// Interaction logic for DataBinding2.xaml
    /// </summary>
    public partial class DataBinding2 : Window
    {
        User jos = new User { Name = "Joske", Age = 23 };
        //User mieke = new User { Name = "Mieke", Age = 21 };

        public DataBinding2()
        {
            InitializeComponent();

            //formuliertje.DataContext = mieke;
            formuliertje.DataContext = jos; 
        }

        private void willyButton_Click(object sender, RoutedEventArgs e)
        {
            jos.Name = "Willy";
        }
    }
}
