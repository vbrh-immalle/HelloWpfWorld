﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace HelloWorld
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            Trace.WriteLine("Aantal parameters meegegeven: " + e.Args.Count());
            foreach(var arg in e.Args)
            {
                Trace.WriteLine(arg);
            }
        }

        private void Application_Activated(object sender, EventArgs e)
        {

        }
    }
}
